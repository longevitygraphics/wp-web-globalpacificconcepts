<?php
	// Loading child theme textdomain
	load_child_theme_textdomain( CURRENT_THEME, CHILD_DIR . '/languages' );
	
	require_once('custom-js.php');
    
    /*-----------------------------------------------------------------------------------*/
    /* Custom Comments Structure
    /*-----------------------------------------------------------------------------------*/
    if ( !function_exists( 'mytheme_comment' ) ) {
    	function mytheme_comment($comment, $args, $depth) {
    		$GLOBALS['comment'] = $comment;
    	?>
    	<li <?php comment_class('clearfix'); ?> id="li-comment-<?php comment_ID() ?>">
    		<div id="comment-<?php comment_ID(); ?>" class="comment-body clearfix">
    			<div class="wrapper">
    				<div class="comment-author vcard">
    					<?php echo get_avatar( $comment->comment_author_email, 80 ); ?>
    					<?php printf('<span class="author">%1$s</span>', get_comment_author_link()) ?>
    				</div>
    				<?php if ($comment->comment_approved == '0') : ?>
    					<em><?php echo theme_locals("your_comment") ?></em>
    				<?php endif; ?>
    				<div class="extra-wrap">
    					<?php comment_text() ?>
    				</div>
    			</div>
    			<div class="wrapper">
    				<div class="reply">
    					<?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
    				</div>
    				<div class="comment-meta commentmetadata"><?php printf('%1$s', get_comment_date()) ?></div>
    			</div>
    		</div>
    <?php }
    }
    
    //------------------------------------------------------
    //  Related Posts
    //------------------------------------------------------
	if(!function_exists('cherry_related_posts')){
		function cherry_related_posts($args = array()){
			global $post;
			$default = array(
				'post_type' => get_post_type($post),
				'class' => 'related-posts',
				'class_list' => 'related-posts_list',
				'class_list_item' => 'related-posts_item',
				'display_title' => true,
				'display_link' => true,
				'display_thumbnail' => true,
				'width_thumbnail' => 140,
				'height_thumbnail' => 144,
				'before_title' => '<h3 class="related-posts_h">',
				'after_title' => '</h3>',
				'posts_count' => 4
			);
			extract(array_merge($default, $args));

			$post_tags = wp_get_post_terms($post->ID, $post_type.'_tag', array("fields" => "slugs"));
			$tags_type = $post_type=='post' ? 'tag' : $post_type.'_tag' ;
			$suppress_filters = get_option('suppress_filters');// WPML filter
			if ($post_tags && !is_wp_error($post_tags)) {
				$args = array(
					"$tags_type" => implode(',', $post_tags),
					'post_status' => 'publish',
					'posts_per_page' => $posts_count,
					'ignore_sticky_posts' => 1,
					'post__not_in' => array($post->ID),
					'post_type' => $post_type,
					'suppress_filters' => $suppress_filters
					);
				query_posts($args);
				if ( have_posts() ) {
					$output = '<div class="'.$class.'">';
					$output .= $display_title ? $before_title.of_get_option('blog_related', theme_locals("posts_std")).$after_title : '' ;
					$output .= '<ul class="'.$class_list.' clearfix">';
					while( have_posts() ) {
						the_post();
						$thumb   = has_post_thumbnail() ? get_post_thumbnail_id() : PARENT_URL.'/images/empty_thumb.gif';
						$blank_img = stripos($thumb, 'empty_thumb.gif');
						$img_url = $blank_img ? $thumb : wp_get_attachment_url( $thumb,'full');
						$image   = $blank_img ? $thumb : aq_resize($img_url, $width_thumbnail, $height_thumbnail, true) or $img_url;

						$output .= '<li class="'.$class_list_item.'">';
						$output .= $display_thumbnail ? '<figure class="thumbnail featured-thumbnail"><a href="'.get_permalink().'" title="'.get_the_title().'"><img data-src="'.$image.'" alt="'.get_the_title().'" /></a></figure>': '' ;
						//$output .= $display_link ? '<a href="'.get_permalink().'" >'.get_the_title().'</a>': '' ;
						$output .= $display_link ? '<h5><a href="'.get_permalink().'" >'.get_the_title().'</a></h5>': '' ;
                        $excerpt = get_the_excerpt();
                        $output .= my_string_limit_words($excerpt,5);
                        $output .= '</li>';
					}
					$output .= '</ul></div>';
					echo $output;
				}
				wp_reset_query();
			}
		}
	}
    
    
     /**
     * Post Grid
     *
     */
    if (!function_exists('posts_grid_shortcode')) {
    
    	function posts_grid_shortcode($atts, $content = null) {
    		extract(shortcode_atts(array(
    			'type'            => 'post',
    			'category'        => '',
    			'custom_category' => '',
    			'tag'             => '',
    			'columns'         => '3',
    			'rows'            => '3',
    			'order_by'        => 'date',
    			'order'           => 'DESC',
    			'thumb_width'     => '370',
    			'thumb_height'    => '250',
    			'meta'            => '',
    			'excerpt_count'   => '15',
    			'link'            => 'yes',
    			'link_text'       => __('Read more', CHERRY_PLUGIN_DOMAIN),
    			'custom_class'    => ''
    		), $atts));
    
    		$spans = $columns;
    		$rand  = rand();
    
    		// columns
    		switch ($spans) {
    			case '1':
    				$spans = 'span12';
    				break;
    			case '2':
    				$spans = 'span6';
    				break;
    			case '3':
    				$spans = 'span4';
    				break;
    			case '4':
    				$spans = 'span3';
    				break;
    			case '6':
    				$spans = 'span2';
    				break;
    		}
    
    		// check what order by method user selected
    		switch ($order_by) {
    			case 'date':
    				$order_by = 'post_date';
    				break;
    			case 'title':
    				$order_by = 'title';
    				break;
    			case 'popular':
    				$order_by = 'comment_count';
    				break;
    			case 'random':
    				$order_by = 'rand';
    				break;
    		}
    
    		// check what order method user selected (DESC or ASC)
    		switch ($order) {
    			case 'DESC':
    				$order = 'DESC';
    				break;
    			case 'ASC':
    				$order = 'ASC';
    				break;
    		}
    
    		// show link after posts?
    		switch ($link) {
    			case 'yes':
    				$link = true;
    				break;
    			case 'no':
    				$link = false;
    				break;
    		}
    
    			global $post;
    			global $my_string_limit_words;
    
    			$numb = $columns * $rows;
    
    			// WPML filter
    			$suppress_filters = get_option('suppress_filters');
    
    			$args = array(
    				'post_type'         => $type,
    				'category_name'     => $category,
    				$type . '_category' => $custom_category,
    				'tag'               => $tag,
    				'numberposts'       => $numb,
    				'orderby'           => $order_by,
    				'order'             => $order,
    				'suppress_filters'  => $suppress_filters
    			);
    
    			$posts      = get_posts($args);
    			$i          = 0;
    			$count      = 1;
    			$output_end = '';
    			$countul = 0;
    
    			if ($numb > count($posts)) {
    				$output_end = '</ul>';
    			}
    
    			$output = '<ul class="posts-grid row-fluid unstyled '. $custom_class .' ul-item-'.$countul.'">';
    			$countul ++;
    
    			foreach ( $posts as $j => $post ) {
    				$post_id = $posts[$j]->ID;
    				//Check if WPML is activated
    				if ( defined( 'ICL_SITEPRESS_VERSION' ) ) {
    					global $sitepress;
    
    					$post_lang = $sitepress->get_language_for_element( $post_id, 'post_' . $type );
    					$curr_lang = $sitepress->get_current_language();
    					// Unset not translated posts
    					if ( $post_lang != $curr_lang ) {
    						unset( $posts[$j] );
    					}
    					// Post ID is different in a second language Solution
    					if ( function_exists( 'icl_object_id' ) ) {
    						$posts[$j] = get_post( icl_object_id( $posts[$j]->ID, $type, true ) );
    					}
    				}
    
    				setup_postdata($posts[$j]);
    				$excerpt        = get_the_excerpt();
    				$attachment_url = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), 'full' );
    				$url            = $attachment_url['0'];
    				$image          = aq_resize($url, $thumb_width, $thumb_height, true);
    				$mediaType      = get_post_meta($post_id, 'tz_portfolio_type', true);
    				$prettyType     = 0;
    
    				if ($count > $columns) {
    					$count = 1;
    					$output .= '<ul class="posts-grid row-fluid unstyled '. $custom_class .' ul-item-'.$countul.'">';
    				}
    				
    
    				$output .= '<li class="'. $spans .' list-item-'.$count.'">';
    					if(has_post_thumbnail($post_id) && $mediaType == 'Image') {
    
    						$prettyType = 'prettyPhoto-'.$rand;
    
    						$output .= '<figure class="featured-thumbnail thumbnail">';
    						$output .= '<a href="'.$url.'" title="'.get_the_title($post_id).'" rel="' .$prettyType.'">';
    						$output .= '<img  src="'.$image.'" alt="'.get_the_title($post_id).'" />';
    						$output .= '<span class="zoom-icon"></span></a></figure>';
    					} elseif ($mediaType != 'Video' && $mediaType != 'Audio') {
    
    						$thumbid = 0;
    						$thumbid = get_post_thumbnail_id($post_id);
    
    						$images = get_children( array(
    							'orderby'        => 'menu_order',
    							'order'          => 'ASC',
    							'post_type'      => 'attachment',
    							'post_parent'    => $post_id,
    							'post_mime_type' => 'image',
    							'post_status'    => null,
    							'numberposts'    => -1
    						) );
    
    						if ( $images ) {
    
    							$k = 0;
    							//looping through the images
    							foreach ( $images as $attachment_id => $attachment ) {
    								$prettyType = "prettyPhoto-".$rand ."[gallery".$i."]";
    								//if( $attachment->ID == $thumbid ) continue;
    
    								$image_attributes = wp_get_attachment_image_src( $attachment_id, 'full' ); // returns an array
    								$img = aq_resize( $image_attributes[0], $thumb_width, $thumb_height, true ); //resize & crop img
    								$alt = get_post_meta($attachment->ID, '_wp_attachment_image_alt', true);
    								$image_title = $attachment->post_title;
    
    								if ( $k == 0 ) {
    									if (has_post_thumbnail($post_id)) {
    										$output .= '<figure class="featured-thumbnail thumbnail">';
    										$output .= '<a href="'.$image_attributes[0].'" title="'.get_the_title($post_id).'" rel="' .$prettyType.'">';
    										$output .= '<img src="'.$image.'" alt="'.get_the_title($post_id).'" />';
    									} else {
    										$output .= '<figure class="featured-thumbnail thumbnail">';
    										$output .= '<a href="'.$image_attributes[0].'" title="'.get_the_title($post_id).'" rel="' .$prettyType.'">';
    										$output .= '<img  src="'.$img.'" alt="'.get_the_title($post_id).'" />';
    									}
    								} else {
    									$output .= '<figure class="featured-thumbnail thumbnail" style="display:none;">';
    									$output .= '<a href="'.$image_attributes[0].'" title="'.get_the_title($post_id).'" rel="' .$prettyType.'">';
    								}
    								$output .= '<span class="zoom-icon"></span></a></figure>';
    								$k++;
    							}
    						} elseif (has_post_thumbnail($post_id)) {
    							$prettyType = 'prettyPhoto-'.$rand;
    							$output .= '<figure class="featured-thumbnail thumbnail">';
    							$output .= '<a href="'.$url.'" title="'.get_the_title($post_id).'" rel="' .$prettyType.'">';
    							$output .= '<img  src="'.$image.'" alt="'.get_the_title($post_id).'" />';
    							$output .= '<span class="zoom-icon"></span></a></figure>';
    						}
    					} else {
    
    						// for Video and Audio post format - no lightbox
    						$output .= '<figure class="featured-thumbnail thumbnail"><a href="'.get_permalink($post_id).'" title="'.get_the_title($post_id).'">';
    						$output .= '<img  src="'.$image.'" alt="'.get_the_title($post_id).'" />';
    						$output .= '</a></figure>';
    					}
    
    					$output .= '<div class="clear"></div>';
    
    					$output .= '<h5><a href="'.get_permalink($post_id).'" title="'.get_the_title($post_id).'">';
    						$output .= get_the_title($post_id);
    					$output .= '</a></h5>';
    
    					if ($meta == 'yes') {
    						// begin post meta
    						$output .= '<div class="post_meta">';
    
    							// post category
    							$output .= '<span class="post_category">';
    							if ($type!='' && $type!='post') {
    								$terms = get_the_terms( $post_id, $type.'_category');
    								if ( $terms && ! is_wp_error( $terms ) ) {
    									$out = array();
    									$output .= '<em>Posted in </em>';
    									foreach ( $terms as $term )
    										$out[] = '<a href="' .get_term_link($term->slug, $type.'_category') .'">'.$term->name.'</a>';
    										$output .= join( ', ', $out );
    								}
    							} else {
    								$categories = get_the_category($post_id);
    								if($categories){
    									$out = array();
    									$output .= '<em>Posted in </em>';
    									foreach($categories as $category)
    										$out[] = '<a href="'.get_category_link($category->term_id ).'" title="'.$category->name.'">'.$category->cat_name.'</a> ';
    										$output .= join( ', ', $out );
    								}
    							}
    							$output .= '</span>';
    
    							// post date
    							$output .= '<span class="post_date">';
    							$output .= '<time datetime="'.get_the_time('Y-m-d\TH:i:s', $post_id).'">' .get_the_date(). '</time>';
    							$output .= '</span>';
    
    							// post author
    							$output .= '<span class="post_author">';
    							$output .= '<em> by </em>';
    							$output .= '<a href="'.get_author_posts_url(get_the_author_meta( 'ID' )).'">'.get_the_author_meta('display_name').'</a>';
    							$output .= '</span>';
    
    							// post comment count
    							$num = 0;
    							$queried_post = get_post($post_id);
    							$cc = $queried_post->comment_count;
                                /*
        							if( $cc == $num || $cc > 1 ) : $cc = $cc.' Comments';
        							else : $cc = $cc.' Comment';
        							endif;
                                */
    							$permalink = get_permalink($post_id);
    							$output .= '<span class="post_comment">';
    							$output .= '<a href="'. $permalink . '" class="comments_link">' . $cc . '</a>';
    							$output .= '</span>';
    						$output .= '</div>';
    						// end post meta
    					}
    					$output .= cherry_get_post_networks(array('post_id' => $post_id, 'display_title' => false, 'output_type' => 'return'));
    					if($excerpt_count >= 1){
    						$output .= '<p class="excerpt">';
    							$output .= my_string_limit_words($excerpt,$excerpt_count);
    						$output .= '</p>';
    					}
    					if($link){
    						$output .= '<a href="'.get_permalink($post_id).'" class="btn btn-primary" title="'.get_the_title($post_id).'">';
    						$output .= $link_text;
    						$output .= '</a>';
    					}
    					$output .= '</li>';
    					if ($j == count($posts)-1) {
    						$output .= $output_end;
    					}
    				if ($count % $columns == 0) {
    					$output .= '</ul><!-- .posts-grid (end) -->';
    				}
    			$count++;
    			$i++;
    
    		} // end for
    		wp_reset_postdata(); // restore the global $post variable
    
    		return $output;
    	}
    	add_shortcode('posts_grid', 'posts_grid_shortcode');
    }
        
    
     /**
     * Service Box
     *
     */
    if (!function_exists('service_box_shortcode')) {
    
    	function service_box_shortcode($atts, $content = null) { 
    		extract(shortcode_atts(
    			array(
    				'title'        => '',
    				'subtitle'     => '',
    				'icon'         => '',
    				'text'         => '',
    				'btn_text'     => '',
    				'btn_link'     => '',
    				'btn_size'     => '',
    				'target'       => '',
    				'custom_class' => ''
    		), $atts));
    		
    		$output =  '<div class="service-box '.$custom_class.'">';
    	
    		if($icon != 'no'){
    			$icon_url = CHERRY_PLUGIN_URL . 'includes/images/' . strtolower($icon) . '.png' ;
    			if( defined ('CHILD_DIR') ) {
    				if(file_exists(CHILD_DIR.'/images/'.strtolower($icon).'.png')){
    					$icon_url = CHILD_URL.'/images/'.strtolower($icon).'.png';
    				}
    			}
                if ($icon == 'icon1') {
                    $output .= '<div class="extrabox"><figure class="icon"><i class="icon-home"></i><span>1</span></figure></div>';
                } elseif ($icon == 'icon2') {
                    $output .= '<div class="extrabox"><figure class="icon"><i class="icon-building"></i><span>2</span></figure></div>';
                } elseif ($icon == 'icon3') {
                    $output .= '<div class="extrabox"><figure class="icon"><i class="icon-umbrella"></i><span>3</span></figure></div>';
                } elseif  ($icon == 'icon4') {
                    $output .= '<div class="extrabox"><figure class="icon"><i class="icon-briefcase"></i><span>4</span></figure></div>'; 
                } elseif ($icon == 'icon5') {
                    $output .= '<figure class="icon"><img src="'.$icon_url.'" alt="" /><span>5</span></figure>';    
                } elseif ($icon == 'icon6') {
                    $output .= '<figure class="icon"><img src="'.$icon_url.'" alt="" /><span>6</span></figure>';    
                } elseif ($icon == 'icon7') {
                    $output .= '<figure class="icon"><img src="'.$icon_url.'" alt="" /><span>7</span></figure>';    
                } elseif ($icon == 'icon8') {
                    $output .= '<figure class="icon"><img src="'.$icon_url.'" alt="" /><span>8</span></figure>';    
                } elseif ($icon == 'icon9') {
                    $output .= '<figure class="icon"><img src="'.$icon_url.'" alt="" /><span>9</span></figure>';    
                } elseif ($icon == 'icon10') {
                    $output .= '<figure class="icon"><img src="'.$icon_url.'" alt="" /><span>10</span></figure>';    
                }                   			
    		}
    
    		$output .= '<div class="service-box_body">';
    
    		if ($title!="") {
    			$output .= '<h2 class="title"><a href="'.$btn_link.'" title="'.$title.'" target="'.$target.'">';
    			$output .= $title;
    			$output .= '</a></h2>';
    		}
    		if ($subtitle!="") {
    			$output .= '<h5 class="sub-title">';
    			$output .= $subtitle;
    			$output .= '</h5>';
    		}
    		if ($text!="") {
    			$output .= '<div class="service-box_txt">';
    			$output .= $text;
    			$output .= '</div>';
    		}
    		if ($btn_text!="") {
    			$output .=  '<div class="btn-align"><a href="'.$btn_link.'" title="'.$btn_text.'" class="btn btn-inverse btn-'.$btn_size.' btn-primary " target="'.$target.'">';
    			$output .= $btn_text;
    			$output .= '</a></div>';
    		}
    		$output .= '</div>';
    		$output .= '</div><!-- /Service Box -->';
    		return $output;
    	}
    	add_shortcode('service_box', 'service_box_shortcode');
    }
    
?>
